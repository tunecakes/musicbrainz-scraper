import logging

import googlemaps

log = logging.getLogger('location')
gmaps = googlemaps.Client(key='AIzaSyCnPHE5S7PEaBzB5oOqJeF4TbViB2AA1Wg')

def geocode(city, state):
  locations = []

  # First try city and state
  if city and state:
    locations = gmaps.geocode(city + ', ' + state)

  # Try only state for location
  if len(locations) is 0 and state:
    log.info('Attempting to geocode %s', state)
    locations = gmaps.geocode(state)

  # Try only city for location
  if len(locations) is 0 and city:
    log.info('Attempting to geocode %s', city)
    locations = gmaps.geocode(city)

  if len(locations) is 0:
    log.warning('No location found for this artist')
    return {}

  location = locations[0]

  #if len(locations) is 1:
  #  location = locations[0]
  #else:
  #  location = _prompt_for_location(locations)

  return {
    'lat': float(location['geometry']['location']['lat']),
    'lon': float(location['geometry']['location']['lng'])
  }


def _prompt_for_location(locations):
  ask = ''
  for i, l in enumerate(locations):
    formatted = l['formatted_address']
    ask += str(i) + ') ' + formatted + '\n'

  ask += 'Please enter the number for the location you want to use: '

  try:
    lIndex = int(input(ask))
  except Exception:
    log.warning('\n\033[93mYou must enter a number for the location\033[0m')
    return _prompt_for_location(locations)

  return locations[lIndex]
