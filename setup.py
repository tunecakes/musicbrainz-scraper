from setuptools import setup

setup(
  name='Tunecakes MusicBrainz Scraper',
  version='0.1',
  description='Scrapes data from MusicBrainz, converts it, and uploads it into Tunecakes',
  url='https://gitlab.com/tunecakes/musicbrainz-scraper',
  author='Brian Schultz',
  author_email='brian@tunecakes.com',
  license='MIT',
  install_requires=[
    'googlemaps',
    'musicbrainzngs',
    'pycountry',
    'requests',
    'unidecode'
  ],
  py_modules=['mb-scraper'],
  entry_points='''
    [console_scripts]
    mb-scraper=scraper:main
  ''',
  zip_safe=False
)
