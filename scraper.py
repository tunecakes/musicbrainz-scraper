"""
Scrapes artist and release data from the internet and adds it to Tunecakes

The user specifies an artist by name and can optionally provide a 2 character country code
to specify the artists country of origin. The program will then search for the artist name
in MusicBrainz. If there is one match, it uses that artist. If there is more than one match,
it will ask the user to specify the artist to process. After getting all artist info it can,
the program will then get all the Albums and EPs it can from MusicBrainz for the given
artist. Finally, it attempts to upload the info into Tunecakes.

This module can process one artist at a time or process a TSV formatted file of artist names
and country codes. If using TSV file, the country code is optional.

Country codes are ISO 3166-1 alpha-2
"""

import argparse
import json
import logging
import os
import sys

import musicbrainzngs as mb
import pycountry

from artist import Artist
from release import Release

def get_args():
  parser = argparse.ArgumentParser(description='Let it be');
  parser.add_argument('-a', '--artist')
  parser.add_argument('-b', '--bulk-file')
  parser.add_argument('-p', '--prompt', action="store_true")
  parser.add_argument('-v', '--verbose', action="store_true")
  
  # Add ISO 3166 countries to arg parser
  country_group = parser.add_mutually_exclusive_group()
  for c in pycountry.countries:
    country_group.add_argument('--' + c.alpha_2, action='store_const', dest='country', const=c.alpha_2, help='Use ' + c.name + ' for country')
  
  return parser.parse_args()

def configure_logging(verbose):
  level = logging.DEBUG if verbose else logging.INFO
  
  logging.basicConfig(level=level)

  locationLogger = logging.getLogger('location')
  locationLogger.setLevel(level)
  
  tunecakesLogger = logging.getLogger('tunecakes')
  tunecakesLogger.setLevel(level)

  releaseLogger = logging.getLogger('release')
  releaseLogger.setLevel(level)

  artistLogger = logging.getLogger('artist')
  artistLogger.setLevel(level)

def main():
  args = get_args()
  configure_logging(args.verbose)

  if sys.version_info < (3, 0):
    logging.error('Python 3 is required for this script')
    sys.exit(1)
  
  if args.bulk_file:
    process_bulk_file(args.bulk_file, args.prompt)
  else:
    if not args.artist:
      raise Exception('You must provide an artist to scrape')

    mb_artists = Artist.search_artists(args.artist, args.country)
    if len(mb_artists) < 1:
      logging.error('No artists found with this info...')
      return
    
    if len(mb_artists) is 1:
      artist = mb_artists[0]
    else:
      artist = prompt_for_artist(mb_artists)
      if not artist:
        print("Skipping this artist...")
        sys.exit(0)

    process_single_artist(artist['id'])


def process_single_artist(mbid):
  a = Artist(mbid)
  releases = Release.generate_releases_for_artist(a)

  for r in releases:
    r.get_album_art()
  
  tc_id = a.upload_to_tunecakes()
  for r in releases:
    r.upload_to_tunecakes(tc_id)


def process_bulk_file(file_path, prompt):
  """
  Bulk files are TSV format without a header.
  There are only 2 columns in the TSV. The first column is the artist,
  the second column is the 2 character country code
  """
  artists_countries = []

  with open(file_path) as f:
    for line in f:
      line = line.strip()
      try:
        a, c = line.split('\t')
        artists_countries.append((a, c))
      except:
        artists_countries.append((line, None))

  # Process the artists
  for a, c in artists_countries:
    print('Processing {} - {}'.format(a, c))
    mb_artists = Artist.search_artists(a, c)

    if len(mb_artists) < 1:
      logging.error('No artists found with info:\n\tARTIST - {}\n\tCOUNTRY - {}'.format(a, c))
      log_failed_artist_lookup(a)
      continue
    elif len(mb_artists) > 1 and not prompt:
      logging.error('Multiple artists found with info:\n\tARTIST - {}\n\tCOUNTRY - {}'.format(a, c))
      log_multiple_artist_matches(a)
      continue
    elif len(mb_artists) > 1 and prompt:
      artist = prompt_for_artist(mb_artists)
      if not artist:
        log_skipped_artist_lookup(a)
        print("Skipping this artist...")
        continue
    else:
      artist = mb_artists[0]
    
    process_single_artist(artist['id'])


LOG_DIR = os.path.join(os.getcwd(), 'bulk_load_output')
FAILURES = os.path.join(LOG_DIR, 'failed_artists.txt')
SKIPS = os.path.join(LOG_DIR, 'skipped_artists.txt')
MULTIPLES = os.path.join(LOG_DIR, 'multiple_results_artists.txt')
os.makedirs(LOG_DIR, exist_ok=True)

def log_failed_artist_lookup(artist):
  with open(FAILURES, 'a') as f:
    f.write('{}\n'.format(artist))

def log_skipped_artist_lookup(artist):
  with open(SKIPS, 'a') as f:
    f.write('{}\n'.format(artist))

def log_multiple_artist_matches(artist):
  with open(MULTIPLES, 'a') as f:
    f.write('{}\n'.format(artist))


def prompt_for_artist(artists):
  """
  Takes a list of artists from the MusicBrainz API and returns user selected artist
  """
  ask = ''
  # Build input test
  for i, a in enumerate(artists):
    name = a['name']
    started = ' Started: "' + a['life-span']['begin'] + '"' if 'begin' in a['life-span'] else ''
    country = ' Country: "' + a['country'] + '"' if 'country' in a else ''
    ask += str(i) + ') Name: "' + a['name']  + '"' + country + started + '\n'
    if 'comment' in a:
      ask += a['comment'] + '\n'

  skip_answer = len(artists)
  ask += "{}) Skip this artist\n".format(skip_answer)

  ask += 'Please enter the number for the artist you want to use: '



  try:
    aIndex = int(input(ask))
  except Exception:
    logging.warning('\n\033[93mYou must enter a number for the artist\033[0m')
    return prompt_for_artist(artists)

  if aIndex == skip_answer:
    return False

  return artists[aIndex]


# Init musicbrainz api
mb.set_useragent('Tunecakes', '0.1', 'brian@tunecakes.com')

if __name__ == '__main__':
  main()
