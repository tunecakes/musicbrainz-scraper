import json
import logging
import os

import requests

log = logging.getLogger('tunecakes')

TC_HOST = os.environ.get('TC_HOST', 'http://localhost:3001')
TC_BASE_URL = TC_HOST + '/api'
TC_USER = os.environ.get('TC_USER', 'brian@tunecakes.com')
TC_PASS = os.environ.get('TC_PASS', 'asdfasdf')

TIMEOUT=5

headers = {}

def login():
  url = TC_BASE_URL + '/authenticate'

  r = requests.post(url, json={ 'email': TC_USER, 'password': TC_PASS }, timeout=2)

  log.debug('Tunecakes auth response %s', r.text)

  if r.status_code > 299:
    raise Exception(r.text)

  body = r.json()

  headers['Authorization'] = 'Bearer {}'.format(body['token'])

def log_headers():
  logging.warning('headers %s', headers)

def get_all_artists():
  r = requests.get(TC_BASE_URL + '/artist', timeout=TIMEOUT)
  return r.json()

def get_artist_by_mbid(mbid):
  params = {
    "query": json.dumps({"mbid": mbid})
  }
  r = requests.get(TC_BASE_URL + '/artist', params=params, timeout=TIMEOUT)
  return r.json()

def get_all_releases(artist_id):
  r = requests.get('{}/artist/{}/release'.format(TC_BASE_URL, artist_id), timeout=TIMEOUT)
  return r.json()

def add_artist(data):
  url = TC_BASE_URL + '/artist'
  
  log.debug('Posting artist to tunecakes %s', data)
  
  r = requests.post(url, json=data, headers=headers, timeout=TIMEOUT)
  log.debug('Tunecakes artist response %s', r.text)
  body = r.json()
  
  if r.status_code == 400 and \
     'errors' in body and \
     'alias' in body['errors'] and \
     body['errors']['alias']['kind'] == 'unique':
    raise ValueError('Alias is not unique')

  if r.status_code > 299:
    raise Exception(body)

  return body['id']

def add_release(artist_id, data):
  url = TC_BASE_URL + '/release'
  data['artist'] = artist_id
  
  log.debug('Posting release to tunecakes %s', data)

  r = requests.post(url, json=data, headers=headers, timeout=TIMEOUT)
  log.debug('Tunecakes release response %s', r.text)
  body = r.json()

  if r.status_code > 299:
    raise Exception(body)

# I log myself in because I'm a big boy
login()
