import json
import logging
import random
import re
import time
from urllib.parse import urlparse

import musicbrainzngs as mb
import requests

import location
import tunecakes

log = logging.getLogger('artist')

class Artist:
  def __init__(self, mbid):
    mb_artist = self.get_artist_by_mbid(mbid)

    log.debug('Musicbrainz artist %s', json.dumps(mb_artist, indent=2))

    self.id = mbid

    self.data = {
      'mbid': mbid,
      'name': mb_artist['name'],
      'alias': re.sub('[^0-9a-z]+', '', mb_artist['name'].lower()),
      'start_date': mb_artist.get('life-span', {}).get('begin'),
      'tags': [],
    }

    self.parse_tag_list(mb_artist.get('tag-list', []))

    for relation in mb_artist.get('url-relation-list', []):
      self._parse_url_relation(relation)

    self.data['location'] = location.geocode(mb_artist.get('begin-area', {}).get('name'),
                                             mb_artist.get('area', {}).get('name'))
    log.info(json.dumps(self.data, indent=2))


  def check_if_exists(self):
    artists = tunecakes.get_artist_by_mbid(self.id)
    for a in artists:
      if a.get('mbid') == self.id:
        return a

    return False


  def upload_to_tunecakes(self):
    try:
      tmp_artist = self.check_if_exists()
      if tmp_artist:
        return tmp_artist['id']
      else:
        return tunecakes.add_artist(self.data)
    except ValueError:
      # Only happens if the alias is not unique
      p = re.compile("(\d+)$")
      match = p.search(self.data['alias'])

      # Add or increment numerical postfix to make alias unique
      try:
        postfix = match.group(1)
        new_alias = re.sub(postfix + '$', '', self.data['alias'])
        new_alias = new_alias + str(int(postfix) + 1)
      except:
        new_alias = self.data['alias'] + '1'

      self.data['alias'] = new_alias
      return self.upload_to_tunecakes()

    except Exception as e:
      logging.warn('Failed to upload {}. Reason: {}'.format(self.data.get('name'), e))

    retries = 0
    while retries < 5:
      wait_time = 2 ** (retries + 1)
      logging.warning('Waiting {} seconds before checking if upload succeeded...'.format(wait_time))
      time.sleep(wait_time)

      try:
        artist = self.check_if_exists()
        if artist:
          logging.info('Upload succeeded')
          return artist['id']
        else:
          return self.upload_to_tunecakes()
      except Exception as e:
        logging.warn('Failed to get artist {} from API. Reason: {}'.format(self.data.get('name'), e))

      retries += 1

    logging.error('Completely failed to upload {}'.format(self.data.get('name')))


  @staticmethod
  def search_artists(artist, country=None):
    """
    Searches the MusicBrainz API for artists with the given name and country of origin
    """
    if country:
      artists = mb.search_artists(artist=artist, country=country, strict=True)
    else:
      artists = mb.search_artists(artist=artist, strict=True)
  
    return artists['artist-list']

  @staticmethod
  def get_artist_by_mbid(mbid):
    return mb.get_artist_by_id(mbid, includes=['url-rels', 'tags'])['artist']


  def parse_tag_list(self, tag_list):
    for tag_object in tag_list:
      if "name" not in tag_object:
        continue
      try:
        if len(tag_object["count"]) < 1:
          continue
      except:
        continue

      tag = tag_object["name"].lower()
      tag = re.sub(r'(?![a-z]).', '-', tag)
      tag = re.sub(r'-+', '-', tag)
      tag = re.sub(r'(^-|-$)', '', tag)

      if len(tag) < 3:
        continue

      self.data['tags'].append(tag)


  '''
  EXAMPLE URL RELATION LIST "url-relation-list": [
    {
      "target": "http://www.modestmouse.com/",
      "type-id": "fe33d22f-c3b0-4d68-bd53-a856badf2b15",
      "type": "official homepage"
    },
    {
      "target": "https://commons.wikimedia.org/wiki/File:Modest_Mouse_UPT.jpg",
      "type-id": "221132e9-e30e-43f2-a741-15afc4c5fa7c",
      "type": "image"
    },
    {
      "target": "https://en.wikipedia.org/wiki/Modest_Mouse",
      "type-id": "29651736-fa6d-48e4-aadc-a557c6add1cb",
      "type": "wikipedia"
    },
    {
      "target": "https://twitter.com/modestmouseband",
      "type-id": "99429741-f3f6-484b-84f8-23af51991770",
      "type": "social network"
    },
    {
      "target": "https://www.facebook.com/ModestMouse",
      "type-id": "99429741-f3f6-484b-84f8-23af51991770",
      "type": "social network"
    },
    {
      "target": "https://open.spotify.com/artist/1yAwtBaoHLEDWAnWR87hBT",
      "type": "streaming music",
      "type-id": "769085a1-c2f7-4c24-a532-2375a77693bd"
    },
    { 
  ### NOTE for youtube: This url does not work with noembed yet, but it is embedable
  ### I submitted an issue here: https://github.com/leedo/noembed/issues/81
      "target": "https://www.youtube.com/user/MODESTMOUSEVEVO",
      "type-id": "6a540e5b-58c6-4192-b6ba-dbc71ec8fcf0",
      "type": "youtube"
    }
  ]
  '''
  def _parse_url_relation(self, relation):
    if relation['type'] == 'social network' and 'facebook' in relation['target'].lower():
      self.data['facebook_url'] = relation['target']
      self.data['facebook_id'] = relation['target'].rsplit('/', 1)[-1]
    elif relation['type'] == 'social network' and 'twitter' in relation['target'].lower():
      self.data['twitter_url'] = relation['target']
      self.data['twitter_id'] = relation['target'].rsplit('/', 1)[-1]
    elif relation['type'] == 'streaming music' and 'spotify' in relation['target'].lower():
      url = relation['target']
      self.data['spotify_id'] = urlparse(url).path.rsplit('/', 1)[1]
      self.data['spotify_url'] = url
    elif relation['type'] == 'image':
      #TODO: Parse other image urls beyond wikimedia (especially because wikimedia format blows)

      if 'image_link' in self.data:
        return
      elif 'archive.org' in relation['target']:
        self.data['image_link'] = relation['target']
      elif 'wikimedia.org' in relation['target']:
        file_name = relation['target'].split('File:')[1]
        url = 'https://en.wikipedia.org/w/api.php?action=query&prop=imageinfo&iiprop=url&titles=File%3A{file_name}&format=json'.format(file_name=file_name)
        try:
          res = requests.get(url).json()
        except:
          log.warning('Unable to parse image url %s', relation['target'])
          return

        pages = res['query']['pages']
        for k in pages:
          if 'imageinfo' in pages[k]:
            self.data['image_link'] = pages[k]['imageinfo'][0]['url']
          return
      else:
        log.warning('Unable to parse image url %s', relation['target'])
