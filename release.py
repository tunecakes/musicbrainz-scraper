import json
import logging
import random
import re
import sys
import time

import musicbrainzngs as mb
import requests
from unidecode import unidecode

import tunecakes

log = logging.getLogger('release')

# 100 is the maximum number of results you get per page
results_per_page = 100

# Amazon Affiliate ID
#  append to amazon url in 'tag' query parameter
amazon_id = 'tunecakes-20'

format_mapping = {
  'CD': 'cd',
  'CD-R': 'cd',
  'Enhanced CD': 'cd',
  'DualDisc': 'cd',
  'Copy Control CD': 'cd',
  'SHM-CD': 'cd',
  'Hybrid SACD': 'cd',
  'Vinyl': 'vinyl',
  '12" Vinyl': 'vinyl',
  'Cassette': 'cassette',
  'Digital Media': 'digital'
}

default_format = 'Other'

class ReleaseParseException(Exception):
  pass


class Release():
  def __init__(self, mbid, artist):
    self.artist = artist
    self.id = mbid
    self.data = {
      'purchase_urls': [],
      'songs': [],
      'mbid': mbid
    }

    mb_release_group = self.get_mb_release_group_by_id(mbid)
    log.debug('Musicbrainz release group %s', json.dumps(mb_release_group, indent=2))

    self.data['type'] = mb_release_group.get('type')
    self.data['publish_date'] = mb_release_group.get('first-release-date')

    if not mb_release_group.get('release-list'):
      log.error('No release-list in this release group: {}'.format(mb_release_group))
      self.upload_to_tunecakes = lambda *args: None
      return


    for r in mb_release_group['release-list']:
      if 'name' not in self.data:
        self.data['name'] = r['title']
      elif unidecode(self.data['name']).lower() != unidecode(r['title']).lower():
        log.warning('Glitch in matrix, different release titles (%s) and (%s)', self.data['name'], r['title'])

      mb_release = self.get_mb_release_info(r['id'])
      log.debug('Musicbrainz release %s', json.dumps(mb_release, indent=2))

      try:
        self.parse_release(mb_release)
      except ReleaseParseException as e:
        log.error('{}: {}'.format(e, mb_release_group))
        self.upload_to_tunecakes = lambda *args: None
        return

    
    log.info('Name: %s', self.data['name'])
    log.info('Songs:\n\t%s', '\n\t'.join(list(s['name'] for s in self.data['songs'])))


  @staticmethod
  def get_mb_release_group_by_id(mbid):
    includes = ['tags', 'url-rels', 'releases']
    mb_release = mb.get_release_group_by_id(mbid, includes=includes)
    return mb_release['release-group']


  '''
  examples of releases from MB
{'id': '53747504-53df-41bc-9ebe-f99568e8d68d', 'primary-type': 'Single', 'type': 'Single', 'title': 'Satellite Skin', 'first-release-date': '2009-05-26'}
{'id': '58cca047-44d7-3193-9935-f76e989abfcb', 'primary-type': 'Single', 'type': 'Single', 'title': 'Never Ending Math Equation', 'first-release-date': '1998-05-05'}
{'id': '5b610505-4987-30fd-a8ba-ef3281766c94', 'primary-type': 'Album', 'type': 'Live', 'first-release-date': '', 'title': '1997-07-18: Atlanta, GA, USA', 'secondary-type-list': ['Live']}
{'id': '5efef28c-ae7c-35fb-acb1-903d870e9a47', 'primary-type': 'Album', 'type': 'Live', 'first-release-date': '', 'title': '2003-07-23: 40 Watt Club, Athens, GA, USA', 'secondary-type-list': ['Live']}
{'id': '5f671ae3-21a3-3958-9e7d-c4492b053cdd', 'primary-type': 'Album', 'type': 'Live', 'first-release-date': '', 'title': '2002-08-15: Diamond Ballroom, Oklahoma City, OK, USA', 'secondary-type-list': ['Live']}
{'id': '613edab0-31d7-4bf2-b567-2f391a59463b', 'primary-type': 'EP', 'type': 'EP', 'title': '5 Track Sampler', 'first-release-date': '2004'}
{'id': '63e42e58-f8df-3e47-944f-00316841abae', 'primary-type': 'Album', 'type': 'Live', 'first-release-date': '', 'title': '2001-02-23: New Orleans, LA, USA', 'secondary-type-list': ['Live']}
{'id': '65b47520-2e58-3bc2-a755-34ff35f23ad5', 'primary-type': 'Album', 'type': 'Album', 'title': 'Good News for People Who Love Bad News', 'first-release-date': '2004-04-06'}
{'id': '668d4a9a-f4d7-3103-b1ac-fb6af025392f', 'primary-type': 'EP', 'type': 'EP', 'title': 'The Fruit That Ate Itself', 'first-release-date': '1997-05-13'}
{'id': '6af4066b-1924-4b9d-8311-766f8ffe8380', 'primary-type': 'Album', 'type': 'Live', 'first-release-date': '', 'title': "1997-04-22: Slim's, Los Angeles, CA, USA", 'secondary-type-list': ['Live']}
{'id': '6c8a8773-48e3-3206-bc43-09e46125813c', 'primary-type': 'Album', 'type': 'Live', 'first-release-date': '2004-04-06', 'title': 'Baron von Bullshit Rides Again', 'secondary-type-list': ['Live']}
{'id': '6ed900ee-377f-4b8e-b425-82d599e24c5f', 'primary-type': 'Other', 'type': 'Other', 'title': 'The Moon & Antarctica Demos', 'first-release-date': '2000'}
  '''

  @classmethod
  def generate_releases_for_artist(self, artist):
    artist_mbid = artist.id
    releases = []
    mb_releases = self.get_releases_for_artist(artist_mbid)

    for r in mb_releases:
      #if r['type'] not in ['Album', 'EP', 'Single']: continue
      if 'type' not in r or r['type'] not in ['Album', 'EP']: continue
      releases.append(self(r['id'], artist))

    return releases

  '''
  Gets all releases for an artist
  '''
  @classmethod
  def get_releases_for_artist(self, artist_mbid, page=0):
    mb_results = mb.browse_release_groups(artist=artist_mbid, limit=results_per_page, offset=page*results_per_page)
    results = mb_results['release-group-list']

    if len(results) < results_per_page:
      return results
    else:
      return results + self.get_releases_for_artist(artist_mbid, page + 1)

  def get_mb_release_info(self, mbid):
    includes = [
      'tags',
      'area-rels',
      'labels',
      'url-rels',
      #'instrument-rels'
      #'artists',
      'media',
      #'artist-credits',
      #'discids',
      #'annotation',
      #'aliases',
      #'artist-rels',
      #'place-rels',
      #'event-rels',
      'recordings',
      'recording-rels',
      'release-rels',
      #'release-group-rels',
      #'series-rels',
      'work-rels',
    ]

    mb_res = mb.get_release_by_id(mbid, includes=includes)
    return mb_res['release']


  def parse_release(self, mb_release):
    '''
    {
      "url-relation-list": [
        {
          "target": "https://www.amazon.com/gp/product/B000003L26",
          "type-id": "4f2e710d-166c-480c-a293-2e2c8d658d87",
          "type": "amazon asin"
        },
        {
          "target": "https://www.discogs.com/release/379811",
          "type-id": "4a78823c-1c53-4176-a5f3-58026c76f2bc",
          "type": "discogs"
        }
      ],
      "release-event-count": 1,
      "packaging": "Jewel Case",
      "date": "1997-11-18",
      "quality": "normal",
      "id": "2a6d8f7c-8cca-4fb1-9f8c-0de137837f0f",
      "status": "Official",
      "text-representation": {
        "language": "eng",
        "script": "Latn"
      },
      "asin": "B000003L26",
      "cover-art-archive": {
        "back": "false",
        "artwork": "true",
        "count": "1",
        "front": "true"
      },
      "country": "US",
      "release-event-list": [
        {
          "area": {
            "id": "489ce91b-6658-3307-9877-795b68554c98",
            "name": "United States",
            "iso-3166-1-code-list": [
              "US"
            ],
            "sort-name": "United States"
          },
          "date": "1997-11-18"
        }
      ],
      "title": "The Lonesome Crowded West",
      "label-relation-list": [
        {
          "type": "copyright",
          "target": "95fc5a0f-96a8-49d7-a982-dfa84e0df5b6",
          "end": "1997",
          "ended": "true",
          "label": {
            "id": "95fc5a0f-96a8-49d7-a982-dfa84e0df5b6",
            "name": "Up Records",
            "sort-name": "Up Records",
            "disambiguation": "independent; Seattle, WA"
          },
          "direction": "backward",
          "begin": "1997",
          "type-id": "2ed5a497-4f85-4b3f-831e-d341ad28c544"
        }
      ],
      "barcode": "796818004421"
    }
    '''
    self.parse_url_list(mb_release)
    self.parse_media_list(mb_release)

  def parse_url_list(self, mb_release):
    if 'url-relation-list' not in mb_release: return

    try:
      mb_format = mb_release.get('medium-list')[0]['format']
      if mb_format not in format_mapping:
        log.warning('Unrecognized media format "%s"', mb_format)
    except:
      log.warning('Unable to get format for release %s', mb_release['id'])
      mb_format = None

    
    fmt = format_mapping.get(mb_format, default_format)

    for url_rel in mb_release['url-relation-list']:
      media_type = url_rel.get('type')
      if media_type == 'cover art link':
        self.data['image_link'] = url_rel.get('target')
      elif media_type == 'amazon asin':
        self.data['purchase_urls'].append({'type': fmt, 'url': url_rel['target'] + '?tag=' + amazon_id})
      elif media_type == 'purchase for download':
        self.data['purchase_urls'].append({'type': fmt, 'url': url_rel['target']})
      elif media_type == 'purchase for mail-order':
        self.data['purchase_urls'].append({'type': fmt, 'url': url_rel['target']})
      elif media_type == 'streaming music':
        log.debug('skipping streaming music url')
      elif media_type == 'discogs':
        log.debug('skipping discog url')
      elif media_type == 'allmusic':
        log.debug('skipping allmusic url')
      elif media_type == 'download for free':
        log.debug('skipping downlaod for free url')
      elif media_type == 'discography entry':
        log.debug('skipping discography entry url')
      elif media_type == 'other databases':
        log.debug('skipping database url')
      else:
        log.warning('Unrecognized url type "%s"\n%s', media_type, url_rel)

  def parse_media_list(self, mb_release):
    if 'medium-list' not in mb_release:
      log.warning('medium-list is not in this release... wtf? %s', mb_release)
      return

    try:
      tracks = mb_release['medium-list'][0].get('track-list', [])
    except:
      raise ReleaseParseException('Failed to get track list')

    for track in tracks:
      song = {
        'name': track['recording']['title'],
        # The length could be anyone of these, and it might be none of them
        'length': int(track.get('length', track.get('track_or_recording_length', track['recording'].get('length', 0)))),
        # Tags are only alphanumeric with periods separating words
        'tags': [re.sub('[^0-9a-zA-Z]+', '-', tag['name'].lower()) for tag in track['recording'].get('tag-list', [])],
        # We choose recording ID because that seems to remain consistent across albums
        'mbid': track['recording']['id']
      }

      self._merge_song_into_release(song, int(track['position']) - 1)

  def get_album_art(self):
    if 'image_link' in self.data:
      return

    url = 'http://coverartarchive.org/release-group/{}'.format(self.id)
    log.info('Getting album art from ' + url)
    res = requests.get(url)
    log.debug('Cover art archive response: %s', res.text)

    if res.status_code != 200:
      return
    
    image_list = res.json()

    for i in image_list['images']:
      if i.get('front'):
        self.data['image_link'] = i['image']
        return
      

  '''
  This method will check to see if this song already exists in the release via the mbid.
  If it doesn't match, we just add it based on the position
  If it does match, we will return
  '''
  def _merge_song_into_release(self, song, position):
    for s in self.data['songs']:
      if s['mbid'] == song['mbid'] or unidecode(s['name']).lower() == unidecode(song['name']).lower():
        # Let's try to update the length
        s['length'] = s['length'] if s['length'] > 0 else song['length']

        # merge tags
        s['tags'] = list(set(s['tags'] + song['tags']))

        # Get outta this joint
        return

    # Dis bitch ass song is new, wtf. y u take up moar space?
    self.data['songs'].insert(position, song)


  def check_if_exists(self):
    logging.info(self.artist.data)
    releases = tunecakes.get_all_releases(self.artist.data['alias'])
    for r in releases:
      if r.get('mbid') == self.id:
        return r

    return False


  def upload_to_tunecakes(self, artist_id):
    try:
      if self.check_if_exists():
        return
      else:
        return tunecakes.add_release(artist_id, self.data)
    except Exception as e:
      log.warn('Failed to upload {}. Reason {}'.format(self.data.get('name'), e))

    retries = 0
    while retries < 5:
      wait_time = 2 ** (retries + 1)
      log.warning('Waiting {} seconds before checking if upload succeeded...'.format(wait_time))
      time.sleep(wait_time)

      try:
        release = self.check_if_exists()
        if release:
          log.info('Upload succeeded')
          return release['id']
        else:
          return self.upload_to_tunecakes(artist_id)
      except Exception as e:
        logging.warn('Failed to get release {} from API. Reason: {}'.format(self.data.get('name'), e))

      retries += 1

    log.error('Completely failed to upload {}'.format(self.data.get('name')))
